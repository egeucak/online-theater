const express = require("express");
const cors = require("cors");

// SERVICES
const uploadService = require("./Upload/UploadService");
const loginService = require("./Login/LoginService");
const registerService = require("./Register/RegisterService");

const router = express.Router();

router.all("*", cors({
    origin: '*',
    methods: ["GET", "POST"],
    allowedHeaders: "*"
}));

router.get("/ping", (req, res) => {
    res.status(200).send("Pong");
});

router
    .route("/upload")
    .post(uploadService.handleUpload);

router
    .route("/login")
    .post(loginService.loginService);

router
    .route("/register")
    .post(registerService.registerService)

module.exports = router;
