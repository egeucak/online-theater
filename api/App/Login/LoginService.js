const bcrypt = require("bcrypt");
const Datastore = require("@google-cloud/datastore");
const datastore = new Datastore({
    projectId: process.env.GCLOUD_PROJECT,
    keyFilename: process.env.GCS_KEYFILE,
});

const loginService = (req, res) => {
    const query = datastore
        .createQuery('memberCredentials')
        .filter('username', '=', req.body.username);
    datastore.runQuery(query)
        .then(results => {
            if (results[0].length !== 1) {
                res.status(401).json({
                    message: 'error',
                    payload: 'wrong username'
                });
            } else {
                const user = results[0][0];
                const passwordMatch = bcrypt.compareSync(req.body.password, user.password);
                console.log(passwordMatch);
                if (!passwordMatch) {
                    res.status(401).json({
                        message: 'error',
                        payload: 'Wrong password',
                    });
                } else {
                    // TODO: CREATE BEARER TOKEN AND SAVE TO REDIS, SEND TOKEN BACK TO CLIENT
                    res.status(200).send(user);
                }
            }
        })
        .catch(err => {
            console.log(err);
            res.status(400).send(err);
        });
}

module.exports = {
    loginService
}
