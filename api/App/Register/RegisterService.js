"use strict";

const uuidv4 = require("uuid/v4");
const bcrypt = require("bcrypt");
const Datastore = require("@google-cloud/datastore");
const datastore = new Datastore({
    projectId: process.env.GCLOUD_PROJECT,
    keyFilename: process.env.GCS_KEYFILE,
});

const registerService = (req, res) => {
    const saltRounds = 9;
    const hash = bcrypt.hashSync(req.body.password, saltRounds);
    console.log(req.body.password, hash);

    const kind = "memberCredentials";
    const memberKey = datastore.key([kind]);
    const member = {
        key: memberKey,
        data: {
            username: req.body.username,
            password: hash,
            email: req.body.email,
            registerDate: new Date(),
            memId: uuidv4()
        }
    }
    datastore
        .save(member)
        .then(() => {
            console.log(`A user has registered succesfully with username ${member.data.username}`);
            res.json({
                message: 'success',
                payload: 'You are succesfully registered. You can login now'
            });
        })
        .catch(err => {
            console.log("ERROR:", err);
            res.send({ message: "Error", err });
        });
}

module.exports = {
    registerService
}