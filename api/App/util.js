const makeid = () => {
    let text = "";
    const from = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i<8; i++){
        text += from.charAt(Math.floor(Math.random() * from.length));
    }

    return text;
}

module.exports = {
    makeid,
}
