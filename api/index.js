require("dotenv");
const express = require("express");
const bodyParser = require('body-parser')
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const port = process.env.PORT || 8080;

const router = require("./App/App");

app.use("/", router);
app.listen(port);

console.log(`App is listening on port ${port}`);
