import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import './App.css';
import Video_Player from './components/VideoPlayer/VideoPlayer';
import Room from './components/Room/Room';
import Header from './components/Header/Header';
import Home from './components/Home/Home';

class App extends Component {
  constructor(props){
    super(props);
  }
    
  state = {
    timestamp: 'no timestamp yet',
    msg: 'no message yet'
  }

  render() {
    console.log("I am app");
    return (
      <Router>
        <div>
          <div className="App">
            <header>
              <Header/>
            </header>
          </div>
          <Home/>
          <footer style={{display: 'flex', justifyContent:'flex-end'}}>
            <p style={{color:'rgba(175, 170, 170, 1)', marginRight: 10}}>
              App version {process.env.REACT_APP_VERSION}
            </p>
          </footer>
        </div>
      </Router>
    );
  }
}

export default App;
