const PATHS = {
    "HOME_PATH":"/",
    "ROOM_PATH":"/rooms/:roomId",
    "UPLOAD_PATH":"/upload",
    "PROFILE_PATH":"/profile",
    "FRIENDS_PATH":"/friends",
    "LOGIN_PATH":"/login",
    "REGISTER_PATH":"/register",
}

const CONSTANTS = {
    "GET_ROOMS_FUNC_URL": "https://us-central1-online-theater.cloudfunctions.net/Get_Rooms",
    "GET_ROOM_INFO_FUNC_URL": "https://us-central1-online-theater.cloudfunctions.net/Get_Room_Info/",
    "UPLOAD_ENDPOINT":"https://us-central1-online-theater.cloudfunctions.net/add-room",
    "UPLOAD_ENDPOINT_LEGACY":"https://backend-dot-online-theater.appspot.com/upload/",
    "LOGIN_ENDPOINT": "https://api-dot-online-theater.appspot.com/login/",
    "REGISTER_ENDPOINT": "https://api-dot-online-theater.appspot.com/register/",
    "PATHS":PATHS,
}

export default CONSTANTS;
