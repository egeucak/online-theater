import firebase from 'firebase/app';
import 'firebase/storage';

const config = {
    apiKey: "AIzaSyA_8rPu09RkjP727kI2Q5CaduCxc3Lomz0",
    authDomain: "online-theater.firebaseapp.com",
    databaseURL: "https://online-theater.firebaseio.com",
    projectId: "online-theater",
    storageBucket: "videos-online-theater",
    messagingSenderId: "144991683916"
}

export default class FBUtils {
    constructor(){
        const app = firebase.initializeApp(config);
        const storage = app.storage();
        this.storageRef = storage.ref();
    }

    _makeid = (len = 8) => {
        let text = "";
        const from = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
        for (let i = 0; i<len; i++){
            text += from.charAt(Math.floor(Math.random() * from.length));
        }
    
        return text;
    }

    upload = (type, file) => {
        return new Promise((resolve, reject) => {
            const targetDir = type;
            const extension = file.name.split(".")[file.name.split(".").length-1];
            const targetFileName = `${targetDir}/${Date.now()}_${this._makeid(6)}.${extension}`;
            const targetRef = this.storageRef.child(targetFileName);
            const uploadTask = targetRef.put(file);
            uploadTask.on('state_changed', snapshot => {
                // Observe state change events such as progress, pause, and resume
                // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                console.log('Upload is ' + progress + '% done');
                switch (snapshot.state) {
                  case firebase.storage.TaskState.PAUSED: // or 'paused'
                    console.log('Upload is paused');
                    break;
                  case firebase.storage.TaskState.RUNNING: // or 'running'
                    console.log('Upload is running');
                    break;
                }
              }, err => {
                  console.error("ERROR AT UPLOADING=> ", err);
                  reject(err);
              }, function() {
                // Handle successful uploads on complete
                // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                  console.log('File available at', downloadURL);
                //   resolve(downloadURL);
                });
                const fileUrl = `https://storage.googleapis.com/${config.storageBucket}/${targetFileName}`;
                resolve(fileUrl);
              });
        });        

    }
}