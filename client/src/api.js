import openSocket from 'socket.io-client';
// const socketAPP = openSocket('https://online-theater-backend.appspot.com');
// const socket = openSocket('http://localhost:8080');

const sockets={
    "socket1": "https://online-theater-backend.appspot.com",
    "socket2": 'http://localhost:8080',
    "socket3": "https://8080-dot-4144267-dot-devshell.appspot.com/?authuser=1",
    "socket4": "https://35.227.68.162:3000/",
    "socket5": "http://35.227.68.162:8080",
    "socket6": "https://back-end-dot-online-theater.appspot.com",
    "socket7": "https://socket-dot-online-theater.appspot.com",
}

console.log("I am imported")

class SocketApi {
    constructor(roomId){
        this.roomId=roomId;
        this.socket = openSocket(sockets["socket7"]);
        this.socket.on('connect', ()=> {
            this.socket.emit('room', this.roomId);
        });
    }
    connectRoom = (cb) => {
        this.socket.on("greet", msg => cb(null, msg));
    }

    sendVideoRequest = (request) => {
        request["room"] = this.roomId;
        console.log("Request to be sent=> ", request);
        this.socket.emit("videoRequestEmit", JSON.stringify(request));
    }

    tellingTime = (time) => {
        this.socket.emit("tellingTime", {time:time, room:this.roomId});
    } 

    getVideoRequest = (cb) => {
        this.socket.on("videoRequest", request => {
            console.log("I sense a request");
            return cb(null, request);
        });
    }
    
    askingTime = (cb) => {
        this.socket.on("askingTime", request => cb(null, request));
    }

    newGuy = (cb) => {
        this.socket.on("newGuy", request => cb(null, request));
    }
}

// const socket = openSocket(sockets["socket2"]);

// const connectRoom = (cb) => { //
//     socket.on("greet", msg => cb(null, msg));
// }

// const getVideoRequest = (cb) => { //
//     socket.on("videoRequest", request => cb(null, request));
// }

// const sendVideoRequest = (request) => { //
//     socket.emit("videoRequestEmit", request);
// }

// const askingTime = (cb) => { //
//     socket.on("askingTime", request => cb(null, request));
// }

// const tellingTime = (time) => {
//     socket.emit("tellingTime", time);
// } 

// const newGuy = (cb) => {
//     socket.on("newGuy", request => cb(null, request));
// }

// export { connectRoom, getVideoRequest, sendVideoRequest, askingTime, tellingTime, newGuy };
export default SocketApi;