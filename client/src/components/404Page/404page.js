import React from "react";
import "./404Page.css";
const errorPage = () => {
  return (
    <body className="errorBody">
      <section id="not-found">
        <div id="title">&bull; 404 Error Page &bull; </div>
        <div class="circles">
          <p>
            404
            <br />><small>PAGE NOT FOUND</small>
          </p>
          <span class="circle big" />
          <span class="circle med" />
          <span class="circle small" />
        </div>
      </section>
    </body>
  );
};

export default errorPage;
