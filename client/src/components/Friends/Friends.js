import React, { Component } from 'react';

import RequireLogin from "../../services/requireLogin/RequireLogin";

export default class Friends extends Component {
    render(){
        return (
            <RequireLogin component={(
                <p>Hello Friends</p>
            )}/>
        );
    }
}
