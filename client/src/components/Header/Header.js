import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { IoIosSearch } from 'react-icons/io';
import { MdFileUpload, MdPerson, MdPeople } from 'react-icons/md';

import CONSTANTS from '../../Consts';

import './Header.css';

class Header extends Component {
    render() {
        return (
            <div className='header'>
                <div className='left'>
                    <h1 style={{ marginLeft: '20px' }} >
                        <Link className='link' to="/">THEATER</Link>
                    </h1>
                </div>
                <div className='middle'>
                    <input placeholder="Search rooms..." className="inputBox" />
                    <IoIosSearch style={{ opacity: '1', marginLeft: '-50px' }} size={'2em'} />
                </div>
                <div className='right' style={{ right: 20 }}>
                    <div className='rightButtons'>
                        <Link className='link' to={CONSTANTS.PATHS.UPLOAD_PATH}><MdFileUpload style={{ marginRight: '30px' }} size={'2em'} /></Link>
                        <Link className='link' to={CONSTANTS.PATHS.FRIENDS_PATH}><MdPeople style={{ marginRight: '30px' }} size={'2em'} /></Link>
                        <Link className='link' to={CONSTANTS.PATHS.PROFILE_PATH}><MdPerson style={{ marginRight: '30px' }} size={'2em'} /></Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;