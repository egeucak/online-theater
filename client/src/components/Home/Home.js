// Dependencies
import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link, Switch, withRouter } from "react-router-dom";
import * as Spinner from 'react-spinkit';

import { connect } from 'react-redux';
import * as actions from '../../redux/actions/index';

// Components
import Room from "../Room/Room";
import App from "../../App";
import errorPage from "../404Page/404page";
import Upload from "../Upload/Upload";
import Profile from '../Profile/Profile';
import Friends from '../Friends/Friends';
import Login from '../Login/Login';
import Register from '../Register/Register';

import CONSTANTS from "../../Consts";

// Styles
import "./Home.css";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  componentDidMount = () => {
    this.props.getRoomList();
  }

  renderRoom = roomData => {
    return (
      <div className="roomContainer" key={roomData.roomId} >
        <Link to={{ pathname:`/rooms/${roomData.roomId}`, state: roomData }}>
          <img src={roomData.image} className="thumb" />
          <div className="roomInfoContainer">
            <div className="roomOwner">{roomData.owner}</div>
            <div className="roomName">{roomData.roomName}</div>
          </div>
        </Link>
      </div>
    );
  };

  renderRoomList = () => {
    return (
      <div className="roomList">
        {this.props.rooms &&
          this.props.rooms.map(room => this.renderRoom(room))}
      </div>
    );
  };

  render() {
    return (
      <Switch>
        <Route exact path={CONSTANTS.PATHS.HOME_PATH} render={() => {
          if (this.props.loading){
            return (
            <div style={{display:'flex',justifyContent:'center', alignItems:'center'}} >
              <Spinner style={{ marginTop: '100px' }} color='white' name="ball-spin-fade-loader" />
            </div>
            );
          }
          this.state.roomOpen && this.setState({roomOpen:false});
          return this.renderRoomList();
          }} />
        <Route path={CONSTANTS.PATHS.ROOM_PATH} exact render={(routeProps) => {
          return <Room {...routeProps} />
        }} />
        <Route path={CONSTANTS.PATHS.UPLOAD_PATH} component={Upload}  exact />
        <Route path={CONSTANTS.PATHS.PROFILE_PATH} component={Profile} exact />
        <Route path={CONSTANTS.PATHS.FRIENDS_PATH} component={Friends} exact />
        <Route path={CONSTANTS.PATHS.LOGIN_PATH} component={Login} exact />
        <Route path={CONSTANTS.PATHS.REGISTER_PATH} component={Register} exact />
        <Route component={errorPage} />
      </Switch>
    );
  }
}

function mapStateToProps(state) {
  return {
    rooms: state.homeReducer.rooms,
    loading: state.homeReducer.loading,
  };
}

export default withRouter(connect(
  mapStateToProps,
  actions  
)(Home));
