import axios from 'axios';

import {
    GET_ROOMS_SUCCESS,
    GET_ROOMS_FAIL
} from './actionTypes';

import CONSTS from '../../../../Consts';

export const getRoomList = () => {
    return (dispatch) => {
        axios
        .get(CONSTS.GET_ROOMS_FUNC_URL)
        .then(response => {
            let result = response.data;
            if (result.message === "success") {
                dispatch({
                    type: GET_ROOMS_SUCCESS,
                    payload: {
                        rooms: result.result,
                        loading: false
                    }
                })
            }
        })
        .catch(err => {
            console.error("Couldn't get the room list=> ", err);
            dispatch({
                type: GET_ROOMS_FAIL,
                payload: err
            });
        });
    }
    
};
