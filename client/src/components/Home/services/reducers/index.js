import {
    GET_ROOMS_SUCCESS,
    GET_ROOMS_FAIL
} from '../actions/actionTypes';

const initialState = {
    rooms: null,
    loading: true,
    error: null
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_ROOMS_SUCCESS:
            return {
                ...state,
                rooms: action.payload.rooms,
                loading: action.payload.loading
            };
        case GET_ROOMS_FAIL:
            return {
                ...state,
                error: action.payload
            }
        default:
            return state;
    }
}