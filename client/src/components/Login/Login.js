import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect,
    withRouter
} from "react-router-dom";

import { connect } from 'react-redux';
import * as actions from '../../redux/actions';

import './Login.css'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null,
        }
    }

    onTextChange = (evt) => {
        this.setState({
            [evt.target.name]: evt.target.value
        });
    }

    onLoginClick = (evt) => {
        console.log(this.state);
        this.props.login(this.state.username, this.state.password);
    }

    render() {
        return (
            <div className="loginPageContainer">
                <div className="loginPage">
                    <div className="fields">
                        <p className="enterCredentials">
                            Enter Your Username and Password to Login
                </p>
                        <input placeholder="Username..." className="inputBoxLogin" name="username" onChange={(evt) => this.onTextChange(evt)} />
                        <input placeholder="Password..." className="inputBoxLogin" name="password" type="password" onChange={(evt) => this.onTextChange(evt)} />
                        <button type="submit" onClick={(evt) => this.onLoginClick(evt)} className="loginButton" >Login</button>
                        <div className="registerNow">
                            <p style={{marginRight: '5px' }}>Don't have an account? You can </p><Link to={{ pathname: `/register` }}> create an account now!</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        isAuthenticated: state.userReducer.isAuthenticated,
    };
}

export default withRouter(connect(
    mapStateToProps,
    actions
)(Login));
