import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect,
    withRouter
} from "react-router-dom";

import { connect } from 'react-redux';
import * as actions from '../../redux/actions';

import './Register.css'
import CONSTANTS from '../../Consts';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null,
            password2: null,
            email: null,
            passwordMismatch: false,
        }
    }

    onTextChange = (evt) => {
        this.setState({
            [evt.target.name]: evt.target.value
        });
        if (evt.target.name === "password2") {
            if (evt.target.value !== this.state.password) {
                this.setState({ passwordMismatch: true });
            } else {
                this.setState({ passwordMismatch: false });
            }
        }
    }

    onLoginClick = (evt) => {
        console.log(this.state);
        if(this.state.password && !this.state.passwordMismatch && this.state.username && this.state.email){
            this.props.register(this.state.username, this.state.email, this.state.password);
        } else {
            alert("We can't register you with missing information");
        }
    }

    render() {
        return (
            <div className="loginPageContainer">
                <div className="loginPage">
                    <div className="fields">
                        <p className="enterCredentials">
                            Choose a username and a password for registering
                        </p>
                        <input placeholder="Username..." className="inputBoxLogin" name="username" onChange={(evt) => this.onTextChange(evt)} />
                        <input placeholder="Email..." className="inputBoxLogin" name="email" onChange={(evt) => this.onTextChange(evt)} />
                        <input placeholder="Password..." className="inputBoxLogin" name="password" type="password" onChange={(evt) => this.onTextChange(evt)} />
                        <input placeholder="Verify password..." className="inputBoxLogin" name="password2" type="password" onChange={(evt) => this.onTextChange(evt)} />
                        { this.state.passwordMismatch && <p className="passwordError">*Passwords do not match</p>}
                        <button type="submit" onClick={(evt) => this.onLoginClick(evt)} className="loginButton" >Register</button>
                        <div className="registerNow">
                            <p style={{ marginRight: '5px' }}>Do you already have an account? You can </p><Link to={{ pathname: CONSTANTS.PATHS.LOGIN_PATH }}> login now!</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        isAuthenticated: state.userReducer.isAuthenticated,
    };
}

export default withRouter(connect(
    mapStateToProps,
    actions
)(Register));
