import React, { Component } from 'react';
import axios from 'axios';

import VideoPlayer from '../VideoPlayer/VideoPlayer';
import SocketApi, { connectRoom } from '../../api';

import CONSTANTS from '../../Consts';

import './Room.css';

class Room extends Component {
    constructor(props){
        super(props);
        this.state = {
            msg: 'Trying to connect...',
            connected: false,
            roomId: props.match.params.roomId,
            roomName: null, //props.location.state.roomName,
            video_url: null, //props.location.state.video_url || null,
        }
        this.connector = new SocketApi(this.state.roomId);
        console.log("Constructing");
        // this.connector = new SocketApi(props.location.state.roomId);
        // this._connectSocks = this._connectSocks.bind(this);
        // connectRoom( (err, msg) => {
        //     this.setState({
        //         msg,
        //         connected: true,
        //     });
        // });    
    }

    componentDidMount = () => {
        this._connectSocks = this._connectSocks.bind(this);
        axios.get( CONSTANTS.GET_ROOM_INFO_FUNC_URL + this.state.roomId)
            .then( response => {
                this.setState({ 
                    video_url:response.data.result[0].video,
                    roomName: response.data.result[0].roomName,
                });
            })
        // this.setState({
        //     video_url: this.props.location.state.url,
        // });        
        this._connectSocks();
        // this.refresh();
    }

    componentWillUnmount = () => {
        clearInterval(this.Interval);
        delete this.connector;
    }

    refresh = () => {
        this.Interval = setInterval(()=>{
            !this.state.connected && window.location.reload();
        }, 1000);
    }

    _connectSocks = () => {
        this.connector.connectRoom( (err, msg) => {
            this.setState({
                msg,
                connected: true,
            });
        }); 
    }

    _renderStatusBar = () => {
        let conColor;
        conColor = this.state.connected ? "green" : "gray";
        return (
        <div style={{ display:'flex', alignItems:'center', justifyContent:'flex-start' }}>
            <span style={{backgroundColor:conColor}} className='dot'/> 
            <p style={{ marginLeft: '10px', color: 'white' }}>{this.state.msg}</p>
        </div>);        
    }

    render() {
        return (
            <div className='roomPage'>
                <div className='roomPageContainer'>
                    <div className='statusBar'>
                        {/* {this.state.msg} */}
                        {this._renderStatusBar()}
                        <p>{this.state.roomName} room</p>
                    </div>

                    <div className='mainContainer'>
                        <div className='playerContainer'>
                            <VideoPlayer video_url={this.state.video_url} connector={this.connector} roomId={this.state.roomId} />
                        </div>
                        <div className='chatContainer'>
                            <p>
                                This place is going to be chat
                            </p>
                        </div>                
                    </div>
                </div>
            </div>
        );
    }
}

const styles={
    statusMsg : {
        fontSize:'large',
    }
}

export default Room;