import React, { Component } from "react";
import Dropzone from 'react-dropzone';
import * as Spinner from 'react-spinkit';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

import FBUtils from '../../Firebase/Storage/utils';

import "./Upload.css";
import CONSTANTS from "../../Consts"

const STEPS = [
    {
        title: "Upload Your Video",
        subtitle: "Start by dragging your first video here, or by clicking this area",
        mimes: "video/*",
        key: "video",
        step: 0
    },
    {
        title: "Upload The Thumbnail",
        subtitle: "Thumbnail will be seen in rooms list",
        mimes: "image/*",
        key: "thumb",
        step: 1
    },
    {
        title: "Enter a name for your watch room",
        subtitle: "",
        mimes: "",
        key: "roomname",
        step: 2
    }
]

class Upload extends Component {
    constructor(props) {
        super(props);
        this.uploadAreaStyle = {};
        this.state = {
            color: 'gray',
            currentStep: 0,
            nextEnabled: false,
            fileName: "",
            video: null,
            thumb: null,
            roomName: "",
            enableSubmit: false,
            loading: false,
            redirect: false,
        }
    }

    componentDidMount = () => {
        this.Uploader = new FBUtils();
    }

    onDrop = (accepted, rejected, step) => {
        const stepName = STEPS[step].key;
        console.log("Accepted=> ", accepted);
        console.log("Rejected=> ", rejected);
        // this.setState({ nextEnabled: true });
        if (accepted.length > 0) {
            this.setState({
                nextEnabled: true,
                fileName: accepted[0].name,
                [stepName]: accepted[0],
            });
        }
        rejected.length > 0 && alert("Drop a valid file");
        rejected.length > 0 && console.log(rejected);
        this.setState({
            color: 'gray',
        })
        console.log(accepted);
    }

    dragOver = () => {
        this.setState({ color: 'lightgray' });
    }

    dragEnd = () => {
        this.setState({ color: 'gray' });
    }

    handleRoomName = (evt) => {
        this.setState({
            roomName: evt.target.value,
            enableSubmit: true,
        });
        console.log(evt.target.value);
    }

    submitVideo = () => {
        console.log(this.state);
        this.setState({
            loading: true,
        })
        Promise.all([
            this.Uploader.upload("video", this.state.video),
            this.Uploader.upload("image", this.state.thumb)
        ]).then((results) => {
            console.log("Upload results=>");
            console.log(results);
            const videoUrl = results[0];
            const thumbUrl = results[1];
            let formData = new FormData();
            formData.append("video", videoUrl);
            formData.append("image", thumbUrl);
            formData.append("owner", "default");
            formData.append("roomName", this.state.roomName);
            axios.post(CONSTANTS.UPLOAD_ENDPOINT, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                }
            }).then( resp => {
                console.log("Success=> ", resp.data);
                this.setState({
                    redirect: resp.data.room,
                    loading: false,
                });
            }).catch( err => {
                console.log("Error=>", err);
            });
        });
    }

    renderSteps = (step) => {
        if (step < 2) {
            return (
                <Dropzone className="uploadContainer"
                    onDrop={(accepted, rejected) => this.onDrop(accepted, rejected, step)}
                    style={{ borderColor: this.state.color }}
                    accept={STEPS[step].mimes}
                    onDragOver={(evt) => {
                        this.dragOver(evt);
                    }}
                    onDragLeave={(evt) => {
                        this.dragEnd(evt);
                    }}
                >
                    <p style={{
                        fontSize: 40,
                        color: this.state.color,
                    }}
                    >
                        {STEPS[step].title}
                    </p>
                    {/* <input type="file"/> */}
                    <p style={{
                        fontSize: 20,
                        color: this.state.color,
                    }}>
                        {STEPS[step].subtitle}
                    </p>

                    <p style={{
                        fontSize: 20,
                        color: this.state.color,
                    }}>
                        {this.state.fileName}
                    </p>
                </Dropzone>
            )
        }
        else if (step === 2) {
            return (
                <div className="uploadContainer" >
                    <p style={{
                        fontSize: 40,
                        color: 'gray'
                    }}>
                        {STEPS[step].title}
                    </p>
                    <input placeholder="Room Name" className="roomInput" onChange={evt => this.handleRoomName(evt)} />
                </div>
            )
        }
    }

    render() {
        if (!this.state.loading) return (
            <div className="parent">
                {this.renderSteps(this.state.currentStep)}
                {this.state.nextEnabled && <input className="nextButton" type="button" value=">" onClick={() => this.setState({ currentStep: ++this.state.currentStep, nextEnabled: false, fileName: "" })} />}
                {this.state.enableSubmit && <input className="submitButton" type="button" value="Create Room" onClick={() => this.submitVideo()} />}
            </div>
        );
        else if (this.state.redirect) return (<Redirect to={this.state.redirect} />);
        else return (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', margin: '50px' }} >
                <p>Uploading Your Video and Creating Your Room</p>
                <Spinner style={{ margin: '100px' }} name="ball-spin-fade-loader" />
                <p>You will be redirected when your room is created</p>
            </div>
        )
    }
}

export default Upload;
