import React, { Component } from "react";
import { Player, ControlBar, CurrentTimeDisplay, Shortcut, FullscreenToggle,
  TimeDivider, VolumeMenuButton, PlayToggle, DurationDisplay, ProgressControl
} from 'video-react';

import _ from 'lodash';

import "./VideoPlayer.css";
import "../../../node_modules/video-react/dist/video-react.css";

import SocketApi, { getVideoRequest, sendVideoRequest, askingTime, tellingTime, newGuy } from '../../api';

const requests = {
  play: function(time) {
    this.seek(time);
    this.play();
  },
  pause: function(time){
    this.seek(time);
    this.pause();
  },
  seek: function(time){
    this.seek(time);
  }
}

class Video_Player extends Component {
  constructor(props){
    super(props);
    this.state={
      // video_url:"https://storage.googleapis.com/test1_egeuu/mov_bbb.mp4",
      videoUrl:this.props.video_url || null,
      roomName: this.props.roomName,
      roomId: this.props.roomId,
      player: null,
      startTime: 0,
      newcomer: true,
    }
    this.player = React.createRef();
    this.connector = this.props.connector;
    // this.connector = new SocketApi(this.props.roomId);
    this.connector.sendVideoRequest({request:"status"});

  }

  componentWillUnmount = () => {
    console.log("Unmounted");
    clearInterval(this.Interval);
  }

  componentDidMount = () => {
    // subscribe state change
    this.Interval = setInterval( ()=> {
      console.log(this.props.video_url)
      if(this.props.video_url){
        this.setState({
          videoUrl: this.props.video_url,
        }, ()=>{
          // this.connector.sendVideoRequest({request:"status"});
          console.log("State is set");
          this.player.current.load();
          this.play();
          clearInterval(this.Interval);
        })
      }
    }, 300);

    this.player.current.subscribeToStateChange(this.handleStateChange.bind(this));  

    this.connector.getVideoRequest( (err, request) => {
      this.handleVideoRequest(request);
    });

    this.connector.askingTime( (err, request) => {
      if (this.state.player){
        this.connector.tellingTime(JSON.stringify({
          time:this.state.player.currentTime,
          paused:this.state.player.paused,
        }));
      }
    });

    if (this.state.newcomer === true) {
      this.connector.newGuy((err, status) => {
        status = JSON.parse(status);
        console.log("New comer=> ",status);
        console.log(this.state.newcomer);
        status && !status.paused && this.player.current.play();
        if (status===null || status.time === 0) return;
        this.seek(status.time);
        this.setState({newcomer:false});
      });
    }

    // TODO: getStartTime from socket and start
  }

  
  handleVideoRequest = (request) => {
    console.log(`Someone has asked me to ${request} the video`);
    let req = JSON.parse(request);
    _.includes(Object.keys(requests), req["request"]) && requests[req["request"]].call(this, req["time"]);
    // (requests.request)();
  }

  handleSeekBarPress = (evt) => {
    this._sendVideoRequest("seek", evt);
    console.log(evt);
  }

  handleStateChange(state, prevState) {
    this.setState({
      player: state,
    });

    if (state.paused!==prevState.paused && state.paused && !state.ended){
      // this.play();
      console.log("Sending pause request");
      this._sendVideoRequest("pause");
    }

    if (state.paused!==prevState.paused && !state.paused){
      // this.pause();
      console.log("Sending play request");
      this._sendVideoRequest("play");
    }
  }

  _sendVideoRequest = (request, time=null) => {
    let req = {
      request: request,
      time: time || (this.state.player && this.state.player.currentTime) || -1,
    };
    this.connector.sendVideoRequest(req);
  }

  play = () => {
    console.log("Pressed play");
    this.player.current.play();
  }

  pause = () => {
    console.log("Pressed pause");
    if(!this.state.player.paused) this.player.current.pause();
  }

  seek = (time) => {
    if (time>0) this.player.current.seek(time);
  }

  handleSeekMouseDown = () => {

  }

  handleSeekMouseUp = (evt) => {
    console.log(evt);
  }

  getVideoState = () => {
    console.log(this.state.player.currentTime);
    // console.log(this.refs.player.state);
    // this.refs.player.play();
    // this.setState({video_state: this.refs.player});
  }

  actionsPlayPause = {
    play: ()=>this.play(),
    pause: ()=>this.pause(),
  }

  actionsSeek = {
    seek: (evt)=>this.handleSeekBarPress(evt),
    handleSeekingTime: ()=>{},
    handleEndSeeking: ()=>{},
  }

  render() {
    // console.log(this.state.videoUrl, this.state.player);
    // console.log("Props=> ", this.props);
    return (
        <Player ref={this.player} aspectRatio={"16:9"} >
          <source src={this.state.videoUrl} />
          <ControlBar autoHide={false} disableDefaultControls={true}>
            <PlayToggle order={1} actions={this.actionsPlayPause}/>
            <VolumeMenuButton order={2}/>
            <CurrentTimeDisplay order={3.1} />
            <TimeDivider order={3.2}/>
            <DurationDisplay order={3.3}/>
            <ProgressControl order={4} actions={this.actionsSeek} />
            <FullscreenToggle order={5} />
            {/* <Shortcut disabled={true} order={99} clickable={false} dbclickable={false}/> */}
          </ControlBar>
        </Player>
    );
  }
}

export default Video_Player;
