import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

// Redux
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise'
import allReducers from './redux/reducers/index';
// import registerServiceWorker from './registerServiceWorker';

// Fonts
import WebFont from 'webfontloader';
WebFont.load({
    google: {
        families: ['Roboto:300,400,500,700']
    }
})

const store = createStore(
    allReducers,
    applyMiddleware(thunk, promise)
);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'));
// registerServiceWorker();
