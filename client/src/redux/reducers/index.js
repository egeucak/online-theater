import homeReducer from '../../components/Home/services/reducers';
import userReducer from '../../components/Profile/services/reducers';

import { combineReducers } from 'redux';

const allReducers = combineReducers({
    homeReducer: homeReducer,
    userReducer: userReducer,
});

export default allReducers;