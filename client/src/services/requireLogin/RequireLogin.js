import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import { connect } from 'react-redux';
import * as actions from '../../redux/actions';

const requireLogin = ({ component: Component, ...rest }) => {
    console.log("Auth => ", this.props.isAuthenticated);
    <Route
     {...rest}
     render={() => {
        this.props.isAuthenticated ? (
            <Component {...this.props} />
        ) : (
            <Redirect
            to={{
                pathname: "/login",
                state: { from: this.props.location }
            }}
            />
        )
     }}
     >
    </Route>
}

class RequireLogin extends Component {
    constructor(props) {
        super(props);
        this.Comp = this.props.component;
    }
    render(){
        if (this.props.isAuthenticated) {
            return <this.Comp {...this.props} />
        } else {
            return <Redirect
                to={{
                    pathname: '/login',
                    state: { from: this.props.location }
                }}
            />
        }
    }
}

function mapStateToProps(state) {
    return {
        isAuthenticated: state.userReducer.isAuthenticated,
    };
  }
  

export default withRouter(connect(
    mapStateToProps,
    actions
)(RequireLogin));
