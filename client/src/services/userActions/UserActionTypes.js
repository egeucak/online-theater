// Login
export const LOGIN_SUCCESS = "login_success";
export const LOGIN_FAILED = "login_failed";

// Register
export const REGISTER_SUCCESS = "register_success";
export const REGISTER_FAILED = "register_failed";
