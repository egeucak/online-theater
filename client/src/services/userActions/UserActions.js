import axios from 'axios';

import {
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    REGISTER_SUCCESS,
    REGISTER_FAILED
} from './UserActionTypes';

import CONSTANTS from '../../Consts';

export function login(username, password) {
    return (dispatch) => {
        axios.post(
            CONSTANTS.LOGIN_ENDPOINT,
            {
                username,
                password
            }
        ).then(response => {
            dispatch({ type: LOGIN_SUCCESS, payload: response });
        }).catch(err => {
            dispatch({ type: LOGIN_FAILED, payload: err });
        });
    }
}

export function register(username, email, password) {
    return (dispatch) => {
        axios.post(
            CONSTANTS.REGISTER_ENDPOINT,
            {
                username,
                email,
                password
            }
        ).then(response => {
            dispatch({ type: REGISTER_SUCCESS, payload: response });
        }).catch(err => {
            dispatch({ type: REGISTER_FAILED, payload: err });
        });
    }
}
