import {
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    REGISTER_SUCCESS,
    REGISTER_FAILED
} from '../userActions/UserActionTypes';

const initialState = {
    authToken: null
}

export default function (state = initialState, action) {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                authToken: action.payload.token,
            };
        case REGISTER_SUCCESS:
            return {
                ...state,
                authToken: action.payload.token,
            }
        default:
            return state;
    }
}