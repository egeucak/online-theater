"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var App = /** @class */ (function () {
    function App() {
        var _this = this;
        this.mountRoutes = function () {
            var router = express.Router();
            router.get("/", function (req, res) {
                res.json({
                    message: "Hello ege",
                });
            });
            _this.express.use("/", router);
        };
        this.express = express();
        this.mountRoutes();
    }
    return App;
}());
exports.default = new App().express;
//# sourceMappingURL=App.js.map