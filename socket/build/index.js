"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sock = require("socket.io");
var _ = require("lodash");
var io = sock();
io.origins('*:*');
var port = process.env.PORT || 8080;
// const testPort = 8081
// app.listen(testPort);
var connectionAmount = 0;
var connectionAmounts = {};
io.on("connection", function (client) {
    // console.log(client);
    var currentRoom;
    client.on('room', function (room) {
        currentRoom = room;
        console.log("Someone wanted to enter the room ", room);
        console.log(connectionAmounts);
        client.join(room);
        _.update(connectionAmounts, room, function (n) { return typeof n === 'number' ? n + 1 : 1; });
        io.to(room).emit("greet", "Someone joined. There are " + connectionAmounts[room] + " active connections.");
    });
    client.on('disconnect', function (resp) {
        _.update(connectionAmounts, currentRoom, function (n) { return --n; });
        console.log("After disc=> ", connectionAmounts);
        // console.log("Disconnect=>", connectionAmounts, " Room=> ", room);
    });
    // connectionAmount++;
    // client.join("room1");
    // io.to("room1").emit(
    //   "greet",
    //   "Someone joined. We have " + connectionAmount + " active connections."
    // );
    // client.on("disconnect", () => {
    //   connectionAmount--;
    //   io.to("room1").emit(
    //     "greet",
    //     "Someone left. We have " + connectionAmount + " active connections."
    //   );
    // });
    client.on("videoRequestEmit", function (request) {
        var temp = JSON.parse(request);
        console.log("Request to server=> ", request);
        if (temp["request"] === "status") {
            if (!temp.room)
                return;
            io.to(temp.room).emit("askingTime", true);
            client.on("tellingTime", function (response) {
                response.room === temp.room && io.emit("newGuy", response.time);
            });
        }
        else {
            io.to(temp.room).emit("videoRequest", request);
        }
    });
});
io.listen(port);
console.log("listening sockets on port " + port);
//# sourceMappingURL=index.js.map