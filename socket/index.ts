import app from "./App/App";
import * as sock from "socket.io";
import * as _ from 'lodash';
let io = sock();
io.origins('*:*');
const port = process.env.PORT || 8080;
// const testPort = 8081
// app.listen(testPort);

let connectionAmount = 0;

let connectionAmounts = {};

io.on("connection", client => {
  // console.log(client);
  let currentRoom;
  client.on('room', room=>{
    currentRoom = room;
    console.log("Someone wanted to enter the room ", room);
    console.log(connectionAmounts);
    client.join(room);
    _.update(connectionAmounts, room, (n)=> typeof n ==='number'  ? n+1 : 1);

    io.to(room).emit(
      "greet",
      `Someone joined. There are ${connectionAmounts[room]} active connections.`
    );
  });

  client.on('disconnect', resp => {
    _.update(connectionAmounts, currentRoom, n => --n );
    console.log("After disc=> ", connectionAmounts);
    // console.log("Disconnect=>", connectionAmounts, " Room=> ", room);
  
  });
  // connectionAmount++;
  // client.join("room1");
  // io.to("room1").emit(
  //   "greet",
  //   "Someone joined. We have " + connectionAmount + " active connections."
  // );

  // client.on("disconnect", () => {
  //   connectionAmount--;
  //   io.to("room1").emit(
  //     "greet",
  //     "Someone left. We have " + connectionAmount + " active connections."
  //   );
  // });

  client.on("videoRequestEmit", request => {
    let temp = JSON.parse(request);
    console.log("Request to server=> ", request);
    if (temp["request"] === "status") {
      if(!temp.room) return;
      io.to(temp.room).emit("askingTime", true);
      client.on("tellingTime", (response) => {
        response.room===temp.room && io.emit("newGuy", response.time);
      });
    } 
    else {
      io.to(temp.room).emit("videoRequest", request);
    }
  });
});

io.listen(port);
console.log(`listening sockets on port ${port}`);
